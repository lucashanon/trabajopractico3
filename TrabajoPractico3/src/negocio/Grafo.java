package negocio;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;



public class Grafo {

	private int[][] Grafo;
	
	public Grafo(int vertices) {
		Grafo=new int[vertices][vertices];
	}

	public void agregarArista(int i, int j)
	{
		verificarVertice(i);
		verificarVertice(j);
		verificarDistintos(i, j);

		Grafo[i][j] = 1;
		Grafo[j][i] = 1;
	}

	public void eliminarArista(int i, int j)
	{
		verificarVertice(i);
		verificarVertice(j);
		verificarDistintos(i, j);

		Grafo[i][j] = 0;
		Grafo[j][i] = 0;
	}

	public boolean existeArista(int i, int j)
	{
		verificarVertice(i);
		verificarVertice(j);
		verificarDistintos(i, j);

		return Grafo[i][j]!=0;
	}

	public int tamanio()
	{
		return Grafo.length;
	}
	
	// Vecinos de un vertice
	public Set<Integer> vecinos(int i)
	{
		verificarVertice(i);
		
		Set<Integer> ret = new HashSet<Integer>();
		for(int j = 0; j < this.tamanio(); ++j) if( i != j )
		{
			if( this.existeArista(i,j) )
				ret.add(j);
		}
		
		return ret;		
	}
	
	public int retornarArista(int x,int y) {
		return Grafo[x][y];
	}
	
	public int cantVecinos(int i){
		int cantidadDeVecinos=0;
		for(int j = 0; j < this.tamanio(); ++j) if( i != j )
		{
			if( this.existeArista(i,j) )
				cantidadDeVecinos++;
		}
		
		return cantidadDeVecinos;		
	}
	
	// Verifica que sea un vertice valido
	private void verificarVertice(int i)
	{
		if( i < 0 )
			throw new IllegalArgumentException("El vertice no puede ser negativo: " + i);
		
		if( i >= Grafo.length )
			throw new IllegalArgumentException("Los vertices deben estar entre 0 y |V|-1: " + i);
	}

	// Verifica que i y j sean distintos
	private void verificarDistintos(int i, int j)
	{
		if( i == j )
			throw new IllegalArgumentException("No se permiten loops: (" + i + ", " + j + ")");
	}
	
	public void generarJSON(String archivo)
	{
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String json = gson.toJson(this);
		try
		{
			FileWriter writer = new FileWriter(archivo);
			writer.write(json);
			writer.close();
		}
		catch(Exception e) {}
	}
	

	public static Grafo leerJSON(String archivo)
	{
		Gson gson = new Gson();
		Grafo ret = null;

		try
		{
			BufferedReader br = new BufferedReader(new FileReader(archivo));
			ret = gson.fromJson(br, Grafo.class);
		}
		catch (Exception e) {  }
		return ret;
	}


}
