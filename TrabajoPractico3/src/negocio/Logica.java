package negocio;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Logica {
	private static Grafo Grafo_G;
	private static List<Integer> conjuntoMinimo;
	private static int[] arrayConCantVecinos;
	private static Set<Integer> verticesEncontrados;
	private static List<Integer> listaDeVerticesDeMayorAMenor;
	
	public static void crearGrafo(int vertices) {
		Grafo_G=new Grafo(vertices);
		arrayConCantVecinos=new int[vertices];
	}

	public static void agregarArista(int vertice1, int vertice2) {
		Grafo_G.agregarArista(vertice1, vertice2);
	}
	
	public static void eliminarArista(int vertice1, int vertice2) {
		Grafo_G.eliminarArista(vertice1, vertice2);
	}
	
	public static int retornarArista(int vertice1, int vertice2) {
		return Grafo_G.retornarArista(vertice1, vertice2);
	}
	
	public static int tamanioGrafo() {
		return Grafo_G.tamanio();
	}

	public static List<Integer> conjuntoDominate(){
		int vertice;
		listaDeVerticesDeMayorAMenor = new ArrayList<>();
		conjuntoMinimo = new ArrayList<>();
		verticesEncontrados=new HashSet<>();
		cargarListaDeCantidades();
		ordenarListaSegunCantidadVecinos();
		while(verticesEncontrados.size()<arrayConCantVecinos.length)
		{
			vertice=listaDeVerticesDeMayorAMenor.get(0);
			listaDeVerticesDeMayorAMenor.remove(Integer.valueOf(vertice));
			conjuntoMinimo.add(vertice);
			agregarVerticeYVecinosAlAuxiliar(vertice);
		}
		return conjuntoMinimo;
	}
	
	private static void cargarListaDeCantidades() {
		for(int i=0;i<arrayConCantVecinos.length;i++) {
			arrayConCantVecinos[i]=Grafo_G.cantVecinos(i);
			listaDeVerticesDeMayorAMenor.add(i);
		}	
	}
	
	private static void ordenarListaSegunCantidadVecinos() {
		Collections.sort(listaDeVerticesDeMayorAMenor, (a, b) -> Integer.compare(arrayConCantVecinos[b], arrayConCantVecinos[a]));
	}
	
	private static void agregarVerticeYVecinosAlAuxiliar(int vertice) {
		verticesEncontrados.add(vertice);
		for(Integer x : Grafo_G.vecinos(vertice)) {
			verticesEncontrados.add(x);
			listaDeVerticesDeMayorAMenor.remove(Integer.valueOf(x));
		}
	}
	
	public static void guardarContenido() {
		Grafo_G.generarJSON("Grafo");
	}
	
	public static void leerArchivo() {
		int tamanioGrafo = Grafo.leerJSON("Grafo").tamanio();
		crearGrafo(tamanioGrafo);
		Grafo_G=Grafo.leerJSON("Grafo");
	}
	

}
	
	



