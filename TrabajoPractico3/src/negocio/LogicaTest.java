package negocio;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;

public class LogicaTest {
	
	private List<Integer> ConjuntoMinimo_Test = new ArrayList<>();
	
	@Before
	public void inicial() {
		Logica.crearGrafo(8);
		Logica.agregarArista(0, 3);
		Logica.agregarArista(0, 3);
		Logica.agregarArista(7, 0);
		Logica.agregarArista(7, 6);
		Logica.agregarArista(7, 2);
		Logica.agregarArista(7, 4);
		Logica.agregarArista(4, 5);
		Logica.agregarArista(5, 1);
		ConjuntoMinimo_Test.add(7);
		ConjuntoMinimo_Test.add(5);
		ConjuntoMinimo_Test.add(3);
	}
	
	@Test
	public void crearGrafoTest() {
		assertEquals(Logica.tamanioGrafo(),8);
	}

	@Test
	public void agregarAristaTest() {
		Logica.agregarArista(7, 1);
		assertEquals(Logica.retornarArista(1, 7),1);
	}
	
	@Test
	public void eliminarAristaTest() {
		Logica.eliminarArista(7, 4);
		assertEquals(Logica.retornarArista(7, 4),0);
	}
	
	@Test
	public void mismoConjuntoDominanteTest() {
		assertEquals(Logica.conjuntoDominate(),ConjuntoMinimo_Test);
	}
	
	@Test
	public void distintoConjuntoDominanteTest() {
		ConjuntoMinimo_Test.add(0);
		assertNotEquals(Logica.conjuntoDominate(),ConjuntoMinimo_Test);
	}
	
	@Test
	public void guardarYLeerContenidoTest() {
		Logica.guardarContenido();
		Logica.crearGrafo(2);
		Logica.leerArchivo();
		assertEquals(Logica.conjuntoDominate(),ConjuntoMinimo_Test);
	}
	
}
