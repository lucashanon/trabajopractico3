package negocio;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

public class GrafoTest {
	
	private Grafo grafo_test = new Grafo(8);
	private Set<Integer> vecinosEsperados = new HashSet<Integer>();
	Set<Integer> vecinosRecibidos = new HashSet<Integer>();
	

	@Before
	public void incializar() {
		grafo_test.agregarArista(0, 3);
		grafo_test.agregarArista(7, 0);
		grafo_test.agregarArista(7, 6);
		grafo_test.agregarArista(7, 2);
		grafo_test.agregarArista(7, 4);
		grafo_test.agregarArista(4, 5);
		grafo_test.agregarArista(5, 1);
	}
	
	@Test
	public void agregar4VecesMismaAristaTest() {
		for (int i=0;i<4;i++) {
			grafo_test.agregarArista(0, 3);
		}
		assertNotEquals(4,grafo_test.retornarArista(0, 3));
	}
	
	@Test
	public void eliminarAristaTest() {
		grafo_test.eliminarArista(0, 3);
		assertEquals(grafo_test.retornarArista(0, 3),0);
	}
	
	@Test
	public void eliminar4VecesMismaAristaTest() {
		for (int i=0;i<4;i++) {
			grafo_test.eliminarArista(7, 0);
		}
		assertEquals(0,grafo_test.retornarArista(7, 0));
	}
	
	@Test
	public void cantidadVecinosTest() {
		assertEquals(grafo_test.cantVecinos(7),4);
	}
	
	@Test
	public void vecinosCorrectos() {
		vecinosEsperados.add(0);
		vecinosEsperados.add(6);
		vecinosEsperados.add(2);
		vecinosEsperados.add(4);
		vecinosRecibidos=grafo_test.vecinos(7);
		assertEquals(vecinosEsperados,vecinosRecibidos);	
	}
	
	@Test
	public void vecinosIncorrectos() {
		vecinosEsperados.add(2);
		vecinosEsperados.add(6);
		vecinosEsperados.add(3);
		vecinosRecibidos=grafo_test.vecinos(7);
		assertNotEquals(vecinosEsperados,vecinosRecibidos);	
	}
	
	@Test
	public void crearJsonTest() {
		grafo_test.generarJSON("Grafo Test");
		grafo_test=null;
		grafo_test=Grafo.leerJSON("Grafo Test");
		assertEquals(grafo_test.retornarArista(0, 3),1);		
	}
	
	

}
