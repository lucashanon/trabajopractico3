package visual;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;

import presenters.Pres_Resultados;


public class Resultados {
	private static JFrame frame;
	private JScrollPane scrollPane;
	private JTextArea textArea;
	private JButton btnCerrar;
	private String cadena=null;
	private List<Integer> list;
	private JButton btnVolver;
	private static boolean estadoGrafo;


	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@SuppressWarnings("static-access")
			public void run() {
				try {
					Resultados window = new Resultados();
					window.frame.setVisible(true);
					window.frame.setLocationRelativeTo(null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}	    
	
	private void incializarPanel(){
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(255, 228, 181));
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 61, 434, 143);
		frame.getContentPane().add(scrollPane);

		textArea = new JTextArea();
		textArea.setBorder(null);
		textArea.setCaretColor(new Color(255, 228, 181));
		textArea.setDisabledTextColor(new Color(0, 0, 0));
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		textArea.setSelectedTextColor(new Color(255, 255, 255));
		textArea.setBackground(new Color(255, 228, 181));
		textArea.setEnabled(false);
		textArea.setEditable(false);
		textArea.setForeground(new Color(0, 0, 0));
		textArea.setFont(new Font("Verdana", Font.PLAIN, 18));
		scrollPane.setViewportView(textArea);

		JLabel lblNewLabel = new JLabel("RESULTADOS");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 24));
		lblNewLabel.setBounds(0, 11, 434, 40);
		frame.getContentPane().add(lblNewLabel);

		btnCerrar = new JButton("CERRAR");
		btnCerrar.setBounds(289, 227, 140, 23);
		frame.getContentPane().add(btnCerrar);
		
		btnVolver = new JButton("VOLVER");
		btnVolver.setBounds(3, 227, 140, 23);
		frame.getContentPane().add(btnVolver);
	}
	
	private void escucharBotonCerrar() {
		btnCerrar.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				if(!estadoGrafo) {
					SalirSinGuardar.main(null);
					frame.disable();
				}else {
					frame.dispose();
				}
				
			}
		});
	}
	
	private void escucharBotonVolver() {
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
				Insertar_Eliminar_Aristas.main(null);
			}
		});
	}
	
	private  void repuesta() {
		list=Pres_Resultados.conjuntoDominateMinimo();
		cadena = "El conjunto ";
		cadena += "{";
		for(int i=0; i<list.size();i++) {
			cadena+=list.get(i)+1;
			if(i<list.size()-1)
				cadena += ",";
		}
		cadena += "}";
		cadena += " es un conjunto dominante mínimo de este grafo.";
		textArea.setText(cadena);
	}
	
	protected static void saberSiElGrafoEstaGuardado(boolean estado) {
		estadoGrafo=estado;
	}
	
	protected static void cerrarVentana() {
		frame.dispose();
	}
	
	@SuppressWarnings("deprecation")
	protected static void habilitarVentana() {
		frame.enable();
	}
	
	/**
	 * Create the application.
	 */
	public Resultados() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		incializarPanel();
		repuesta();
		escucharBotonCerrar();
		escucharBotonVolver();
	}

}
