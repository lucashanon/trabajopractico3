package visual;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;

import presenters.Pres_Insertar_Eliminar;

public class Insertar_Eliminar_Aristas {

	private static JFrame frame;
	private JTextField PrimerVertice;
	private JTextField SegundoVertice;
	private JButton btnAgregar;
	private JButton btnResultado;
	private JButton btnVerGrafo;
	private JButton btnGuardar;
	private boolean guardado;
	private JButton btnEliminar;
	private JButton btnVolver;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@SuppressWarnings("static-access")
			public void run() {
				try {
					Insertar_Eliminar_Aristas window = new Insertar_Eliminar_Aristas();
					window.frame.setVisible(true);
					window.frame.setLocationRelativeTo(null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	private void limpiarEntrada() {
		PrimerVertice.setText("");
		SegundoVertice.setText("");
		guardado=false;
	}
	private void incializarPanel() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(255, 228, 181));
		frame.setBounds(100, 100, 473, 343);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel titulo = new JLabel("AGREGAR O ELIMINAR ARISTAS");
		titulo.setHorizontalAlignment(SwingConstants.CENTER);
		titulo.setFont(new Font("Tahoma", Font.PLAIN, 24));
		titulo.setBounds(0, 15, 457, 48);
		frame.getContentPane().add(titulo);

		PrimerVertice = new JTextField();
		PrimerVertice.setHorizontalAlignment(SwingConstants.RIGHT);
		PrimerVertice.setFont(new Font("Tahoma", Font.PLAIN, 18));
		PrimerVertice.setBounds(372, 116, 75, 39);
		frame.getContentPane().add(PrimerVertice);
		PrimerVertice.setColumns(10);

		JLabel lblColoquePrimerVertice = new JLabel("COLOQUE EL PRIMER VERTICE");
		lblColoquePrimerVertice.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		lblColoquePrimerVertice.setBounds(9, 116, 358, 39);
		frame.getContentPane().add(lblColoquePrimerVertice);

		JLabel lblColoqueSegundoVertice_1 = new JLabel("COLOQUE EL SEGUNDO VERTICE");
		lblColoqueSegundoVertice_1.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		lblColoqueSegundoVertice_1.setBounds(9, 170, 343, 39);
		frame.getContentPane().add(lblColoqueSegundoVertice_1);

		SegundoVertice = new JTextField();
		SegundoVertice.setHorizontalAlignment(SwingConstants.RIGHT);
		SegundoVertice.setFont(new Font("Tahoma", Font.PLAIN, 18));
		SegundoVertice.setColumns(10);
		SegundoVertice.setBounds(372, 170, 75, 39);
		frame.getContentPane().add(SegundoVertice);

	}
	private void CrearBotones(){
		btnAgregar = new JButton("AGREGAR");
		btnAgregar.setBackground(new Color(0, 255, 0));

		btnAgregar.setBounds(307, 224, 140, 23);
		frame.getContentPane().add(btnAgregar);


		btnResultado = new JButton("RESULTADO");
		btnResultado.setBounds(158, 224, 140, 23);
		frame.getContentPane().add(btnResultado);

		btnVerGrafo = new JButton("VER GRAFO");
		btnVerGrafo.setToolTipText("");
		btnVerGrafo.setBounds(158, 262, 140, 23);
		frame.getContentPane().add(btnVerGrafo);

		JLabel lblNewLabel = new JLabel("EL GRAFO ACTUAL TIENE " + Pres_Insertar_Eliminar.tamanioGrafo() + " ELEMENTOS.");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNewLabel.setBounds(9, 78, 399, 23);
		frame.getContentPane().add(lblNewLabel);
		
		btnGuardar = new JButton("GUARDAR");
		btnGuardar.setBounds(9, 224, 140, 23);
		frame.getContentPane().add(btnGuardar);
		
		btnEliminar = new JButton("ELIMINAR");
		btnEliminar.setBackground(new Color(255, 0, 0));
		btnEliminar.setBounds(307, 262, 140, 23);
		frame.getContentPane().add(btnEliminar);
		
		btnVolver = new JButton("VOLVER");
		btnVolver.setBounds(9, 262, 140, 23);
		frame.getContentPane().add(btnVolver);
	}
	private void escucharBotonAgregar() {
		btnAgregar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {	
					if (PrimerVertice.getText().equals("") || SegundoVertice .getText().equals("")) {
						JOptionPane.showMessageDialog(null, "Debe poner los vertices");
					}else if(!PrimerVertice.getText().equals("") || !SegundoVertice .getText().equals("")) {
						Pres_Insertar_Eliminar.agregarArista(Integer.parseInt(PrimerVertice.getText())-1,Integer.parseInt(SegundoVertice .getText())-1);
						limpiarEntrada();	
						;}} catch (Exception e1) {JOptionPane.showMessageDialog(null, "NO SE PERMITEN LOOPS O NUMEROS FUERA DE RANGO");}}});
	}

	private void escucharBotonVerGrafo() {
		btnVerGrafo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				VerGrafo.main(null);
			}
		});
	}
	private void escucharBotonResultados() {
		btnResultado.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {	
					Resultados.saberSiElGrafoEstaGuardado(guardado);
					Resultados.main(null);
					frame.dispose();	
				} catch (Exception e1) {JOptionPane.showMessageDialog(null, "no se permiten loops o nuemeros mayores al tamaño del grafo");}}});
		}

	private void escucharBotonGuardar() {
		btnGuardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {	
					Pres_Insertar_Eliminar.guardarGrafo();
					guardado=true;
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(null, "ERROR");
				}
			}});
	}
	
	private void escucharBotonEliminar() {
		btnEliminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {	
					if (PrimerVertice.getText().equals("") || SegundoVertice .getText().equals("")) {
						JOptionPane.showMessageDialog(null, "Debe poner los vertices");
					}else if(!PrimerVertice.getText().equals("") || !SegundoVertice .getText().equals("")) {
						Pres_Insertar_Eliminar.eliminarArista(Integer.parseInt(PrimerVertice.getText())-1,Integer.parseInt(SegundoVertice .getText())-1);
						limpiarEntrada();	
						;}} catch (Exception e1) {JOptionPane.showMessageDialog(null, "NO SE PERMITEN LOOPS O NUMEROS FUERA DE RANGO");}}});
	}
	
	private void escucharBotonVolver() {
		btnVolver.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				try {
					if(!guardado) {
						VolverSinGuardar.main(null);
						frame.disable();
					}else {
						Inicio.main(null);
						frame.dispose();
					}
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(null, "ERROR");
				}
			}});
	}
	
	protected static void cerrarVentana() {
		frame.dispose();
	}
	
	@SuppressWarnings("deprecation")
	protected static void habilitarVentana() {
		frame.enable();
	}
	
	/**¨´
	 * Create the application.
	 */



	public Insertar_Eliminar_Aristas() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		incializarPanel();
		CrearBotones();
		escucharBotonAgregar();
		escucharBotonVerGrafo();
		escucharBotonResultados();
		escucharBotonGuardar();
		escucharBotonEliminar();
		escucharBotonVolver();

	}


}
