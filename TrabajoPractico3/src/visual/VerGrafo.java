package visual;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;

import presenters.Pres_Ver_Grafo;

public class VerGrafo {

	JFrame frame;
	private JScrollPane scrollPane;
	private JTextArea txtrPrueba;
	private JButton btnCerra;
	private String cadena="";

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VerGrafo window = new VerGrafo();
					window.frame.setVisible(true);
					window.frame.setLocationRelativeTo(null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	private  void repuesta() {
		iniciarCadena();
		for (int i = 0; i <  Pres_Ver_Grafo.tamanioGrafo(); i++) {
			cadena+=i+1;
			cadena+=" ";
			for (int j = 0; j <  Pres_Ver_Grafo.tamanioGrafo(); j++) {
				cadena+=Pres_Ver_Grafo.retornarArista(i, j);
				cadena+=" ";
			}  
			cadena+="\n";
		}
		txtrPrueba.setText(cadena);
	}
	
	private void iniciarCadena() {
		cadena+="   ";
		for (int i=0;i<Pres_Ver_Grafo.tamanioGrafo();i++) {
			cadena+=i+1;
			cadena+=" ";
		}
		cadena+="\n";
	}
	
	private void incializarPanel(){
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(255, 228, 181));
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 61, 434, 143);
		frame.getContentPane().add(scrollPane);

		txtrPrueba = new JTextArea();
		txtrPrueba.setBorder(null);
		txtrPrueba.setDisabledTextColor(new Color(0, 0, 0));
		txtrPrueba.setBackground(new Color(255, 228, 181));
		txtrPrueba.setEnabled(false);
		txtrPrueba.setEditable(false);
		txtrPrueba.setForeground(Color.BLACK);
		txtrPrueba.setFont(new Font("Verdana", Font.PLAIN, 18));
		scrollPane.setViewportView(txtrPrueba);

		JLabel lblNewLabel = new JLabel("GRAFO CREADO");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 24));
		lblNewLabel.setBounds(0, 10, 434, 40);
		frame.getContentPane().add(lblNewLabel);

		btnCerra = new JButton("Cerrar");
		btnCerra.setBounds(173, 227, 89, 23);
		frame.getContentPane().add(btnCerra);
	}
	private void escucharBotonCerrar() {
		btnCerra.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
			}
		});
	}
	/**
	 * Create the application.
	 */
	public VerGrafo() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		incializarPanel();
		escucharBotonCerrar() ;
		repuesta();
	}

}
