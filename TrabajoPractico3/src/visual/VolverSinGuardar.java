package visual;

import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import presenters.Pres_Guardar;

import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.SwingConstants;

public class VolverSinGuardar extends JDialog {

	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private static JButton btnGuardar;
	private static JButton btnNoGuardar;
	private static JButton btnCancelar;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			VolverSinGuardar dialog = new VolverSinGuardar();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void iniciarlizarPanel() {
		setBounds(100, 100, 450, 157);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBackground(new Color(255, 228, 181));
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		JLabel lblMensaje = new JLabel("¿QUIERES GUARDAR LOS CAMBIOS DEL GRAFO?");
		lblMensaje.setHorizontalAlignment(SwingConstants.CENTER);
		lblMensaje.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblMensaje.setBounds(0, 15, 434, 50);
		contentPanel.add(lblMensaje);
		
		btnGuardar = new JButton("GUARDAR");
		btnGuardar.setActionCommand("OK");
		btnGuardar.setBounds(3, 80, 140, 23);
		contentPanel.add(btnGuardar);
		
		btnNoGuardar = new JButton("NO GUARDAR");
		btnNoGuardar.setActionCommand("OK");
		btnNoGuardar.setBounds(146, 80, 140, 23);
		contentPanel.add(btnNoGuardar);
		
		btnCancelar = new JButton("CANCELAR");
		btnCancelar.setActionCommand("OK");
		btnCancelar.setBounds(289, 80, 140, 23);
		contentPanel.add(btnCancelar);
	}
	
	private void escucharBotonGuardar() {
		btnGuardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {	
					Pres_Guardar.guardarGrafo();
					Insertar_Eliminar_Aristas.cerrarVentana();
					dispose();
					Inicio.main(null);
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(null, "ERROR");
				}
			}});
	}
	
	private void escucharBotonNoGuardar() {
		btnNoGuardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Insertar_Eliminar_Aristas.cerrarVentana();
					dispose();
					Inicio.main(null);
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(null, "ERROR");
				}
			}});
	}
	
	private void escucharBotonCancelar() {
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					dispose();
					Insertar_Eliminar_Aristas.habilitarVentana();
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(null, "ERROR");
				}
			}});
	}

	/**
	 * Create the dialog.
	 */
	public VolverSinGuardar() {
		setType(Type.POPUP);

		iniciarlizarPanel();
		escucharBotonGuardar();
		escucharBotonNoGuardar();
		escucharBotonCancelar();
	}
}
