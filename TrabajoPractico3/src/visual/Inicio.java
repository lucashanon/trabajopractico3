package visual;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import presenters.Pres_Inicio;


public class Inicio {

	private JFrame Inicio;
	private JTextField tamGrafo;
	private JButton btnRestar = new JButton("-");
	private JButton btnSumar = new JButton("+");
	private JButton btnContinuar = new JButton("CONTINUAR");
	private final JButton btnCrearGrafo = new JButton("CREAR GRAFO");
	private final JButton btnVerGrafo = new JButton("VER GRAFO");
	private final JButton btnLeerGrafo = new JButton("CARGAR GRAFO");


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Inicio window = new Inicio();
					window.Inicio.setVisible(true);
					window.Inicio.setLocationRelativeTo(null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	private void incializarPanel() {
		Inicio = new JFrame();
		Inicio.setTitle("TRABAJO PRACTICO N°3");
		Inicio.getContentPane().setBackground(new Color(255, 228, 181));
		Inicio.setBounds(100, 100, 492, 300);
		Inicio.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Inicio.getContentPane().setLayout(null);

		JLabel txtIndicacion = new JLabel("TAMAÑO DEL GRAFO");
		txtIndicacion.setFont(new Font("Tahoma", Font.PLAIN, 24));
		txtIndicacion.setHorizontalAlignment(SwingConstants.CENTER);
		txtIndicacion.setBounds(0, 11, 476, 41);
		Inicio.getContentPane().add(txtIndicacion);

		tamGrafo = new JTextField();
		tamGrafo.setText("4");
		tamGrafo.setHorizontalAlignment(SwingConstants.CENTER);
		tamGrafo.setBounds(206, 104, 64, 20);
		Inicio.getContentPane().add(tamGrafo);
		tamGrafo.setColumns(10);

		JPanel buttonPane = new JPanel();
		Inicio.getContentPane().add(buttonPane, BorderLayout.SOUTH);
		buttonPane.setLayout(null);
	}

	private void crearBotones() {
		btnSumar.setBounds(322, 103, 41, 23);
		Inicio.getContentPane().add(btnSumar);
		btnRestar.setBounds(113, 103, 41, 23);
		Inicio.getContentPane().add(btnRestar);	
		btnContinuar.setBounds(322, 193, 140, 23);
		btnContinuar.setEnabled(false);
		Inicio.getContentPane().add(btnContinuar);
		btnCrearGrafo.setBounds(14, 193, 140, 23);
		Inicio.getContentPane().add(btnCrearGrafo);
		btnVerGrafo.setBounds(168, 227, 140, 23);
		btnVerGrafo.setEnabled(false);
		Inicio.getContentPane().add(btnVerGrafo);
		btnLeerGrafo.setBounds(168, 193, 140, 23);
		
		Inicio.getContentPane().add(btnLeerGrafo);
	}

	private void escucharBotonRestar() {
		btnRestar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {		
					int tam=Integer.parseInt(tamGrafo.getText());
					if(tam>1){
						tamGrafo.setText((--tam)+"");
					}

				} catch (Exception e) {JOptionPane.showMessageDialog(null, "ERROR");}		
			}		
		});
	}



	private void escucharBotonSumar() {
		btnSumar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {		
					int tam=Integer.parseInt(tamGrafo.getText());
					tamGrafo.setText((++tam)+"");
				}

				catch (Exception e) {JOptionPane.showMessageDialog(null, "ERROR");}		
			}		
		});
	}


	private void escucharBotonContinuar() {
		btnContinuar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					Insertar_Eliminar_Aristas.main(null);
					Inicio.dispose();}
				catch (Exception e) {
					JOptionPane.showMessageDialog(null, "ERROR");
					}
				}
			});
	}

	private void escucharBotonCrearGrafo() {
		btnCrearGrafo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					int tam=Integer.parseInt(tamGrafo.getText());
					Pres_Inicio.crearGrafo(tam);
					btnCrearGrafo.setEnabled(false);
					btnLeerGrafo.setEnabled(true);
					habilitarBtnVerYContinuar_Notificar("GRAFO CREADO CORRECTAMENTE");
				}
				catch (Exception e) {JOptionPane.showMessageDialog(null, "ERROR");}		
			}		
		});
	}

	private void escucharBotonVerGrafo() {
		btnVerGrafo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					VerGrafo.main(null);
				}
				catch (Exception e) {JOptionPane.showMessageDialog(null, "Debe crear el grafo para poder verlo");
				}
			}
		});
	}

	private void escucharBotonLeerGrafo() {
		btnLeerGrafo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					Pres_Inicio.cargarGrafo();
					btnLeerGrafo.setEnabled(false);
					btnCrearGrafo.setEnabled(true);
					habilitarBtnVerYContinuar_Notificar("GRAFO CARGADO CORRECTAMENTE");
				}catch (Exception e) {JOptionPane.showMessageDialog(null, "ERROR");
				}
			}
		});
	}
	
	private void habilitarBtnVerYContinuar_Notificar(String mensaje) {
		btnContinuar.setEnabled(true);
		btnVerGrafo.setEnabled(true);
		JOptionPane.showMessageDialog(null, mensaje);
	}
	
	/**
	 * Create the application.
	 */
	public Inicio() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		incializarPanel();
		crearBotones();
		escucharBotonRestar();
		escucharBotonSumar();
		escucharBotonCrearGrafo();
		escucharBotonContinuar();
		escucharBotonVerGrafo();
		escucharBotonLeerGrafo();


	}

}
