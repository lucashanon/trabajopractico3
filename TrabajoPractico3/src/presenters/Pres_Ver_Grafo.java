package presenters;

import negocio.Logica;

public class Pres_Ver_Grafo {
	
	public static int retornarArista(int i, int j) {
		return Logica.retornarArista(i, j);
	}
	
	public static int tamanioGrafo() {	
		return Logica.tamanioGrafo();
	}
	
}
