package presenters;

import negocio.Logica;

public class Pres_Insertar_Eliminar {

	public static void agregarArista(int vertice1, int vertice2){
		Logica.agregarArista(vertice1, vertice2);
	}
	
	public static void eliminarArista(int i, int j) {
		Logica.eliminarArista(i, j);
	}

	public static void guardarGrafo() {
		Logica.guardarContenido();
	}
	
	public static int tamanioGrafo() {	
		return Logica.tamanioGrafo();
	}
	
}
